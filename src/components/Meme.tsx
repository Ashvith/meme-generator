import { useState, useEffect } from 'react'

function Meme() {
  const [meme, setMeme] = useState({
    topText: '',
    bottomText: '',
    randomImage: 'http://i.imgflip.com/1bij.jpg',
  })

  const [allMemes, setAllMemes] = useState([])

  useEffect(() => {
    fetch('https://api.imgflip.com/get_memes')
      .then((res) => res.json())
      .then((data) => setAllMemes(data.data.memes))
  }, [])

  function getMemeImage() {
    const randomNumber = Math.floor(Math.random() * allMemes.length)
    const url = allMemes[randomNumber].url
    setMeme((prevMeme) => ({
      ...prevMeme,
      randomImage: url,
    }))
  }

  function handleChange(event) {
    const { name, value } = event.target
    setMeme((prevMeme) => ({
      ...prevMeme,
      [name]: value,
    }))
  }

  return (
    <main>
      <div className='form'>
        <input
          type='text'
          className='form--input'
          placeholder='Top text'
          name='topText'
          onChange={handleChange}
          value={meme.topText}
        />
        <input
          type='text'
          className='form--input'
          placeholder='Bottom text'
          name='bottomText'
          onChange={handleChange}
          value={meme.bottomText}
        />
        <input
          type='button'
          className='form--button'
          value='Get a new meme image 🖼️'
          onClick={getMemeImage}
        />
      </div>
      <div className='meme'>
        <img src={meme.randomImage} className='meme--image' />
        <h2 className='meme--text top'> {meme.topText}</h2>
        <h2 className='meme--text bottom'>{meme.bottomText}</h2>
      </div>
    </main>
  )
}

export default Meme
